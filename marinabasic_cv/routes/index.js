var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	res.render('index');
});

router.get('/hr', function(req, res) {
	res.render('index_hr');
});

router.get('/aboutme', function(req, res) {
	res.render('index');
});

router.get('/omeni', function(req, res) {
	res.render('index_hr');
});

router.get('/education', function(req, res) {
	res.render('education');
});

router.get('/obrazovanje', function(req, res) {
	res.render('education_hr');
});

router.get('/projects', function(req, res) {
	res.render('projects');
});

router.get('/projekti', function(req, res) {
	res.render('projects_hr');
});

module.exports = router;
